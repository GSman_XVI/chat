# Chat App #

### Installation ###

```
#!cmd
git clone https://GSman_XVI@bitbucket.org/GSman_XVI/chat.git
```

```
#!cmd
cd chat
```

```
#!cmd
npm install
```

```
#!cmd
npm install bower -g
```

```
#!cmd
bower install
```

```
#!cmd
npm install gulp -g
```

```
#!cmd
gulp
```

```
#!cmd
npm start
```

```
#!cmd
go to localhost:3000
```