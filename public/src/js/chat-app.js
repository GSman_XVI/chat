import {Message} from './message';

export class ChatApp {
    constructor() {
        this.username = 'Guest';
        //Создаем объект для связя с сервером через WebSocket.
        this.socket = io();

        //Действие при входящем сообщении.
        this.socket.on('message', msg => {
            this.showMessage(msg);
        });

        //Действие при обновлении списка пользователей чата.
        this.socket.on('users', users => {
            this.showUsers(users);
        });

        //Действие при получении дополнительной информации (вход, выход, смена ника).
        this.socket.on('info', info => {
            this.showInfo(info);
        });

        let chat = document.forms.chat;
        chat.onsubmit = () => this.onMessageSend();
        let user = document.forms.username;
        user.onsubmit = () => this.onUsernameChanged();
    }

    onMessageSend() {
        let input = document.forms.chat.msg;
        //Создаем объект класса Message и передаем через WebSocket, инициируя на сервере событие 'message'.
        let msg = new Message(input.value, this.username);
        this.socket.emit('message', msg);
        input.value = '';
        return false;
    }

    onUsernameChanged() {
        let input = document.forms.username.name;
        this.username = input.value;
        //Передаем чере WebSocket новое имя пользователя, инициируя на сервере событие 'username'.
        this.socket.emit('username', this.username);
        document.querySelector('#myname').innerHTML = this.username;
        return false;
    }

    //Метод, отображающий новое сообщение в чате.
    showMessage(msg) {
        let ul = document.querySelector('#text > ul');
        let li = document.createElement('li');
        //Получаем дату в нужном нам формате.
        let date = new Date(new Date().getTime()).toLocaleTimeString();
        //Добавляем в li дату, имя пользователя и сообщение по данному шаблону.
        li.innerHTML = `<span>[${date}] ${msg.user}: </span>${msg.message}`;
        ul.appendChild(li);
        let text = document.querySelector('#text');
        //Скроллим список сообщений вниз.
        text.scrollTop = text.scrollHeight;
    }

    //Метод, отображающий информацию о входе, выходе и смене имени пользователя в чате.
    showInfo(info) {
        let ul = document.querySelector('#text > ul');
        let li = document.createElement('li');
        li.className += 'info';
        li.innerHTML = info;
        ul.appendChild(li);
        let text = document.querySelector('#text');
        //Скроллим список сообщений вниз.
        text.scrollTop = text.scrollHeight;
    }

    //Метод, отображающий список пользователей чата. 
    //Вызывается при каждом обновлении списка пользователей.
    showUsers(users) {
        //Удаляем старые элементы из списка.
        let ul = document.querySelector('#contacts > ul');
        while (ul.firstChild) {
            ul.removeChild(ul.firstChild);
        }
        for (let user of users) {
            let li = document.createElement('li');
            li.innerHTML = user[1];
            ul.appendChild(li);
        }
    }
}
