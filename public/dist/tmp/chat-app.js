'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ChatApp = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _message = require('./message');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChatApp = exports.ChatApp = function () {
    function ChatApp() {
        var _this = this;

        _classCallCheck(this, ChatApp);

        this.username = 'Guest';
        this.socket = io();

        this.socket.on('message', function (msg) {
            _this.showMessage(msg);
        });

        this.socket.on('users', function (users) {
            _this.showUsers(users);
        });

        this.socket.on('info', function (info) {
            _this.showInfo(info);
        });

        var chat = document.forms.chat;
        chat.onsubmit = function () {
            return _this.onMessageSend();
        };
        var user = document.forms.username;
        user.onsubmit = function () {
            return _this.onUsernameChanged();
        };
    }

    _createClass(ChatApp, [{
        key: 'onMessageSend',
        value: function onMessageSend() {
            var input = document.forms.chat.msg;
            var msg = new _message.Message(input.value, this.username);
            this.socket.emit('message', msg);
            input.value = '';
            return false;
        }
    }, {
        key: 'onUsernameChanged',
        value: function onUsernameChanged() {
            var input = document.forms.username.name;
            this.username = input.value;
            this.socket.emit('username', this.username);
            document.querySelector('#myname').innerHTML = this.username;
            return false;
        }
    }, {
        key: 'showInfo',
        value: function showInfo(info) {
            var ul = document.querySelector('#text > ul');
            var li = document.createElement('li');
            li.className += 'info';
            li.innerHTML = info;
            ul.appendChild(li);
            var text = document.querySelector('#text');
            text.scrollTop = text.scrollHeight;
        }
    }, {
        key: 'showMessage',
        value: function showMessage(msg) {
            var ul = document.querySelector('#text > ul');
            var li = document.createElement('li');
            var date = new Date(new Date().getTime()).toLocaleTimeString();
            li.innerHTML = '<span>[' + date + '] ' + msg.user + ': </span>' + msg.message;
            ul.appendChild(li);
            var text = document.querySelector('#text');
            text.scrollTop = text.scrollHeight;
        }
    }, {
        key: 'showUsers',
        value: function showUsers(users) {
            var ul = document.querySelector('#contacts > ul');
            while (ul.firstChild) {
                ul.removeChild(ul.firstChild);
            }
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = users[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var user = _step.value;

                    var li = document.createElement('li');
                    li.innerHTML = user[1];
                    ul.appendChild(li);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }]);

    return ChatApp;
}();