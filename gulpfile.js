var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var rename = require('gulp-rename');
var del = require('del');

gulp.task('watch', ['sass'], function () {
    browserSync.init({
        proxy: 'localhost:3000'
    });
    gulp.watch("./public/src/sass/*.sass", ['sass']);
    gulp.watch("./public/src/js/*.js", ['ES6']);
    gulp.watch("./public/dist/css/*.css").on('change', browserSync.reload);
    gulp.watch("./public/dist/js/*.js").on('change', browserSync.reload);
    gulp.watch("./public/index.html").on('change', browserSync.reload);
});

gulp.task('clean-tmp', function(){
  return del(['public/dist/tmp']);
});

gulp.task('babel', function(){
  return gulp.src(['public/src/js/*.js','public/src/js/**/*.js'])
    .pipe(babel())
    .pipe(gulp.dest('public/dist/tmp'));
});

gulp.task('ES6', ['babel'], function(){
  return browserify(['public/dist/tmp/main.js']).bundle()
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(rename('app.js'))
    .pipe(gulp.dest('./public/dist/js'));
});

gulp.task('sass', function () {
    return gulp.src("./public/src/sass/*.sass")
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat("styles.min.css"))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("./public/dist/css"));
});

gulp.task('bower_libs', function () {
    gulp.src([
        'bootstrap/dist/css/bootstrap.min.css',
        'bootstrap/dist/js/bootstrap.min.js',
        'jquery/dist/jquery.min.js',
    ], { cwd: "bower_components/**" })
        .pipe(gulp.dest("./public/libs"));
});

gulp.task('npm_libs', function () {
    gulp.src([
        'socket.io-client/socket.io.js'
    ], { cwd: "node_modules/**" })
        .pipe(gulp.dest("./public/libs"));
});

gulp.task('default', ['npm_libs', 'bower_libs', 'sass', 'ES6']);
