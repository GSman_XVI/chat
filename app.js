var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var socket_io = require('socket.io');

var routes = require('./routes/index');

var app = express();
//Создаем объект для связя с клиентом через WebSocket.
var io = socket_io();
app.io = io;

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes);

//Список пользователей с уникальными идентификаторами.
var users = new Map();

//Обработчик реагирует на подключение нового пользователя.
//Оно происходит автоматически, при открытии страницы.
io.on('connection', function(socket) {
    //Получаем и сохраняем уникальный идентификатор пользователя, сгенерированный socket.io.
    var userId = socket.id;
    //По умолчанию каждый новый пользователь имеет ник Guest.
    users.set(userId, 'Guest');
    //Передаем на клиент обновленный список пользователей.
    io.emit('users', Array.from(users));
    //Передаем информацию о подключении нового пользователя.
    io.emit('info', "User Guest connected.");

    //Обработчик отключения пользователя от сервера.
    socket.on('disconnect', function(data) {
        //Передаем информацию об отключении пользователя.
        //Используя сохраненный ранее идентификатор показываем имя пользователя.
        io.emit('info', `User ${users.get(userId)} disconnected.`);
        users.delete(userId);
        //Передаем на клиент обновленный список пользователей.
        io.emit('users', Array.from(users));
    });

    //При получении сообщения сервер рассылает его всем клиентам.
    socket.on('message', function(msg) {
        io.emit('message', msg);
    });

    //Обработчик изменения имени пользователя.
    socket.on('username', function(name) {
        //Информируем клиент о том, что пользователь сменил ник.
        io.emit('info', `User ${users.get(userId)} changed name to ${name}.`);
        users.set(userId, name);
        //Передаем на клиент обновленный список пользователей.
        io.emit('users', Array.from(users));
    });
});

module.exports = app;
